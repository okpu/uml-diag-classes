# UML - Représentation d'associations dans un diagramme de classes

Une association est l'expression d'un lien entre instance(s). 

Pour représenter, dans un diagramme de classes, des liaisons entre instances, UML offre deux alternatives : notation par **attribut** et notation par **association**. Exemples ci-dessous.

Considérons qu'un projet est sous la responsabilité d'un technicien et d'un commercial.


## Version représentation par associations

```plantuml
class Projet {
  nom : String
}
class Personne {
  nom : String
}

Projet " *  " -> "\t\t\t1\n respTechnique" Personne  :"                                "
Projet " *  " -> "\t\t\t1\n respCommercial" Personne :"                                "

hide circle
```
C'est la représentation **la plus adaptée à une approche conceptuelle**.

On comprend aisément que, dès lors qu'un concept fait l'objet d'une représentation sous forme de classe (c'est le cas en général des objets du domaine), la représentation par une association s'impose (sinon à quoi sert la représentation ?)


## Version représentation par attributs

```plantuml
class Projet {
  nom : String
  respTechnique : Personne
  respCommercial : Personne
}
hide circle
```
Version correcte dans la mesure où l'on souhaite se concentrer essentiellement sur Projet (plus pauvre que la précédente)

## Version représentation par associations exagérées 

```plantuml
class Projet {
  nom : String
}
class Personne {
  nom : String
}
class String {
  chars : char[*]
}
Projet " *  " -> "\t\t\t1\n respTechnique" Personne  :"                                "
Projet " *  " -> "\t\t\t1\n respCommercial" Personne :"                                "
Projet "  " --> "1\n nom" String  :"                                "
Personne "  " --> "1\n nom" String  :"                                "

note Bottom of String
  Il va sans dire que la classe String n'apporte rien à ce type de modèle !
       (dans la mesure où il se focalise sur le domaine métier)
end note

hide circle
```


## Exemple d'une mauvaise pratique, par mélange des représentations


```plantuml
class Projet {
  nom : String
  respCommercial : Personne
}
class Personne {
  nom : String
}

Projet " * " -> "\t\t\t1\n respTechnique" Personne  :"                                "  
note bottom of Projet #red
    incohérence de représentation
end note

hide circle
```
Pas faux en soi, mais la communication est quelque peu brouillée.


## Exemple d'une erreur de modélisation (de conception ?) 


```plantuml
class Projet {
  nom : string
  respTechnique : Personne
  respCommercial : Personne
}
class Personne {
  nom : string
}

Projet " * " -> "\t\t\t1\n respTechnique" Personne  :"                                "
Projet " * " -> "\t\t\t1\n respCommercial" Personne :"                                "

note bottom of Projet #red
    duplication d'associations
    un projet a combien de responsables, 4 ou 2 ?
end note

hide circle
```

Ce dernier diagramme dénote en fait **4 associations**, par effet de bord de mauvaises pratiques.


## Résumé

Des liens entre instances peuvent être notés selon deux 2 syntaxes (par attribut ou par association), mais faire usage de ces deux syntaxes, pour un même "classificateur" dans un même diagramme n'est pas recommandé pour une question de lisibilité. 

Dès qu’un classificateur est présenté sous forme d’une classe dans le diagramme, alors la notation « association » s’impose d’elle-même, sinon pourquoi représenter la classe dans ce diagramme ?

Dans le cas contraire, lorsque le classificateur n’a pas de représentation graphique (pas de classe dans le diagramme) alors la notation « attribut » s’impose d’elle-même aussi, **dès lors où l’on souhaite communiquer cette relation**, car l'exhaustivité n'est en rien obligatoire dans un diagramme de classe. 

Toute autre représentation (hybride ou redondante) serait soit une mauvaise pratique soit une erreur de modélisation.

Voir aussi : 
* [UML SPECS - meta-modèles et nombreux exemples - ~800 pages](https://www.omg.org/spec/UML/2.5.1/PDF)
* [UML Best Practice: Attribute or Association - une argumentation qui s'appuie sur les specs](https://bellekens.com/2011/08/10/uml-best-practice-attribute-or-association/)
* [StackOverFlow attribute or assosication - pour une réponse concise et pragmatique](https://stackoverflow.com/questions/23261380/uml-class-diagram-attribute-or-association)
